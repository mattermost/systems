# WIP: Reporting Standards 

WIP write-up of our reporting standards, which we should make public when complete. 

## Principles 

To be respectful of our colleague's time in meetings and reviews, we use the following prioritized principles when sharing charts, graphs and reports: 

### 1) Unambiguous

There should not be a reasonable way to misinterpret data if all itles, labels and captions are read. E.g. Quarters should be unambiguous as to calendar or fiscal measure. Charts should not have ambiguity around revenue measures, e.g. Bookings, GAAP revenue, ARR, TCV, ACV, purchases, etc. including whether measure include non-profit or academic revenue. 

### 2) Correct 

When practical, avoid having reviewers spot data errors and mistakes (including typos) before data presenter notes them. E.g. note data errors in introduction or on the specific report. 

### 3) Concise 

When practical, use shorter labels and unambiguous abberviations. E.g. MMM YY for monthly labels.  
