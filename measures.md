# Measures 

Work-in-progress summary of reporting metrics used at Mattermost, Inc.  

## Revenue Measures 

### GAAP Revenue

Revenue from sales of licenses or services earned and recognized by Mattermost finance through the delivery of licensed product or services over time under U.S. GAAP accounting rules. 

