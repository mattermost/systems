# README

This is a work-in-progress repo to make more of Mattermost's internal systems and processes more publicly accessible. 

Summary of Resources:  

- [Mattermost Core Team Handbook](https://docs.mattermost.com/guides/core.html) - Procedures and operations of Mattermost community and R&D teams. This is published via Sphinx and managed in a GitHub repo. 
- [Mattermost Internal Operations](https://github.com/mattermost/internal-ops/blob/master/README.md) - Confidential operations at Mattermost (e.g. Analyst Relations with contact info, etc. ). This site is managed in a GitHub repo. 
- [Mattermost Systems (this site)](https://gitlab.com/mattermost/systems/blob/master/README.md) - Work-in-progress documentation of Mattermost Systems outside of community and R&D. We may at some point migrate the core team handbook here after we can get this repo to render nicely in an HTML template. Managed in a GitLab repo. 